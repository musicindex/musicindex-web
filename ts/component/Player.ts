import App from "../App.js";
import YouTubeScreen from "../screen/YouTubeScreen.js";

class Player {

  init() {
    let app = App.i;
    let player = document.getElementById("player");

    let previousSongButton = player.querySelector("#previous-song-button");
    previousSongButton.addEventListener("click", async () => {
      await app.previousSong();
    });

    let playButton = player.querySelector("#play-button");
    playButton.addEventListener("click", () => {
      app.togglePlaying();
    });

    let nextSongButton = player.querySelector("#next-song-button");
    nextSongButton.addEventListener("click", async () => {
      await app.nextSong();
    });

    let deselectButton = player.querySelector("#deselect-button");
    deselectButton.addEventListener("click", async () => {
      await app.changeSong(null);
    });

    let youtubeButton = player.querySelector("#youtube-button");
    youtubeButton.addEventListener("click", async () => {
      if (app.currentScreen instanceof YouTubeScreen) {
        app.popScreen();
      } else {
        await app.pushScreen(new YouTubeScreen());
      }
    });

    let volumeSlider = <HTMLInputElement>player.querySelector("#volume-slider");
    volumeSlider.addEventListener("change", () => app.updateVolume());
    volumeSlider.addEventListener("mousemove", () => app.updateVolume());
    app.updateVolume();

    let timeSlider = <HTMLInputElement>player.querySelector("#time-slider");
    function updateTime() {
      let time = parseFloat(timeSlider.value) * app.music.getDuration();
      if (!isNaN(time) && isFinite(time)) {
        app.music.setTime(time);
      }
    }
    timeSlider.addEventListener("change", updateTime);
    timeSlider.addEventListener("mousedown", () => {
      updateTime();
      timeSlider.setAttribute("pressed", "true");
    });
    timeSlider.addEventListener("mouseup", () => {
      updateTime();
      timeSlider.removeAttribute("pressed");
    });
    timeSlider.addEventListener("mousemove", e => {
      if (timeSlider.getAttribute("pressed") === "true") {
        updateTime();
      }
    });
    updateTime();
  }
};

export default Player;
