import App from "../App.js";

class Sidebar {
  deselect() {
    let nodes = document.getElementsByClassName("sidebar-row");
    for (let node of nodes) {
      node.classList.remove("active");
    }
  }



  private makeRow(hash: string, label: string, icon?: string) {
    let that = this;
    let row = document.createElement("div");
    row.classList.add("sidebar-row");
    row.addEventListener("click", () => {
      location.hash = hash;
      that.deselect();
      row.classList.add("active");
    });

    if (icon !== undefined) {
      let span = document.createElement("span");
      span.classList.add("material-icons");
      span.classList.add("md-24");
      span.innerText = icon;
      row.appendChild(span);
    }
    {
      let span = document.createElement("span");
      span.innerText = label;
      row.appendChild(span);
    }

    return row;
  }



  init() {
    let sidebar = document.getElementById("sidebar");
    sidebar.innerHTML = "";

    {
      let header = document.createElement("h3");
      header.innerText = "Main";
      sidebar.appendChild(header);
    }
    {
      let section = document.createElement("div");
      section.classList.add("sidebar-section");
      section.appendChild(this.makeRow("#home", "Home", "home"));
      section.appendChild(this.makeRow("#test", "Test", "bug_report"));
      section.appendChild(this.makeRow("#youtube", "YouTube Player", "tv"));
      section.appendChild(this.makeRow("#search", "Search", "search"));
      sidebar.appendChild(section);
    }

    {
      let header = document.createElement("h3");
      header.innerText = "Playlists";
      sidebar.appendChild(header);
    }
    {
      let section = document.createElement("div");
      section.id = "sidebar-playlists";
      section.classList.add("sidebar-section");
      sidebar.appendChild(section);
    }

    {
      let header = document.createElement("h3");
      header.innerText = "Add";
      sidebar.appendChild(header);
    }
    {
      let section = document.createElement("div");
      section.classList.add("sidebar-section");
      section.appendChild(this.makeRow("#addSong",   "Add song",   "add"));
      section.appendChild(this.makeRow("#addArtist", "Add artist", "add"));
      section.appendChild(this.makeRow("#addAlbum",  "Add album",  "add"));
      sidebar.appendChild(section);
    }

    this.updatePlaylistHTML();
  }

  updatePlaylistHTML() {
    let section = document.getElementById("sidebar-playlists");
    section.innerHTML = "";

    for (let [key, value] of App.i.playlistStorage.allItems) {
      section.appendChild(this.makeRow(`#playlist#${encodeURIComponent(key)}`, value.name, "playlist_play"));
    }
  }
};

export default Sidebar;
