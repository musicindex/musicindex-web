import App from "../../App.js";
import Logger from "../../util/Logger.js";
import Player from "./Player.js";

declare global {
  interface Window {
    onYouTubeIframeAPIReady: any;
    YT: any
  }
}

class YouTubePlayer extends Player {
  player: any = null;

  getDuration(): number {
    if (this.player !== null) {
      return this.player.getDuration();
    } else {
      return 0;
    }
  }

  getTime(): number {
    if (this.player !== null) {
      return this.player.getCurrentTime();
    } else {
      return 0;
    }
  }

  setTime(time: number, load: boolean): void {
    if (this.player !== null) {
      return this.player.seekTo(time, load);
    }
  }

  play(): void {
    if (this.player !== null) {
      return this.player.playVideo();
    }
  }

  pause(): void {
    if (this.player !== null) {
      return this.player.pauseVideo();
    }
  }

  setVolume(volume: number): void {
    if (this.player !== null) {
      return this.player.setVolume(volume * 100);
    }
  }

  override destruct(): void {
    document.querySelector("#player #controls #youtube-button").classList.remove("active");
    if (this.player !== null) {
      this.player.destroy();
    }
  }

  constructor(id: string) {
    super();
    YouTubePlayer.init().then(() => {

      let YT = window.YT;
      let player = new YT.Player("youtube-player", {
        height: 360,
        width: 640,
        videoId: id,
        playerVars: {
          autoplay: 1,
          controls: 0,
          disablekb: 1
        },
        events: {
          onReady: (event: any) => {
            Logger.log("YouTube player: onPlayerReady", event);
            this.player = player;
            this.setVolume(App.i.volume);

            document.querySelector("#player #controls #youtube-button").classList.add("active");
          },

          onStateChange: (event: any) => {
            Logger.log("YouTube player: onPlayerStateChange", event);
          }
        }
      });
    });
  }


  private static isInitialized = false;
  private static init() {
    return new Promise((resolve, reject) => {
      if (YouTubePlayer.isInitialized) {
        resolve(null);
        return;
      }

      Logger.log("YouTube player init");

      window.onYouTubeIframeAPIReady = () => {
        Logger.log("YouTube player: onYouTubeIframeAPIReady");
        YouTubePlayer.isInitialized = true;
        resolve(null);
      };

      // Load script
      let tag = document.createElement("script");
      tag.src = "https://www.youtube.com/iframe_api";
      let firstScriptTag = document.getElementsByTagName("script")[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    });
  }
}

export default YouTubePlayer;
