import Player from "./Player.js";

class URLPlayer extends Player {
  audio = new Audio();

  getDuration(): number {
    return this.audio.duration;
  }

  getTime(): number {
    return this.audio.currentTime;
  }

  setTime(time: number, load: boolean): void {
    this.audio.currentTime = time;
  }

  play(): void {
    this.audio.play();
  }

  pause(): void {
    this.audio.pause();
  }

  setVolume(volume: number): void {
    this.audio.volume = volume;
  }

  override destruct(): void {
    this.audio.pause();
    this.audio.remove();
  }

  constructor(url: string) {
    super();
    this.audio = new Audio(url);
  }
}

export default URLPlayer;
