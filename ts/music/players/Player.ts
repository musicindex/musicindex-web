abstract class Player {
  
  // Get song duration (in seconds)
  abstract getDuration(): number;

  // Get current time (in seconds)
  abstract getTime(): number;

  // Set current time (in seconds)
  abstract setTime(time: number, load: boolean): void;

  // Set current volume (value from 0 to 1)
  abstract setVolume(volume: number): void;

  // Pause playback
  abstract pause(): void;

  // Unpause playback
  abstract play(): void;

  destruct() {
    
  }
};

export default Player;
