import App from "../App.js";
import URLSource from "../data/sources/URLSource.js";
import YouTubeSource from "../data/sources/YouTubeSource.js";
import Logger from "../util/Logger.js";
import Player from "./players/Player.js";
import URLPlayer from "./players/URLPlayer.js";
import YouTubePlayer from "./players/YouTubePlayer.js";

class MusicManager {
  player: Player;
  volume: number;

  #isPlaying = false;

  getDuration() {
    if (this.player !== undefined) {
      return this.player.getDuration();
    } else {
      return 0;
    }
  }

  getTime() {
    if (this.player !== undefined) {
      return this.player.getTime();
    } else {
      return 0;
    }
  }

  setTime(time: number, load: boolean = true) {
    if (this.player !== undefined) {
      this.player.setTime(time, load);
    }
  }

  setVolume(volume: number) {
    this.volume = volume;
    if (this.player !== undefined) {
      this.player.setVolume(volume);
    }
  }

  play() {
    if (this.player !== undefined) {
      this.player.play();
      this.#isPlaying = true;
    } else {
      this.#isPlaying = false;
    }
  }

  pause() {
    if (this.player !== undefined) {
      this.player.pause();
    }
    this.#isPlaying = false;
  }

  isPlaying() {
    if (this.player === undefined) {
      this.#isPlaying = false;
    }
    return this.#isPlaying;
  }

  isPlayable() {
    return this.player !== undefined;
  }

  async changeSong(id: string) {
    let song = await App.i.songStorage.getItem(id);
    Logger.log("Playing song: ", id, song);

    this.changePlayer(undefined);

    // Find playable source
    let loaded = false;
    if (song !== null && song.sources instanceof Array) {
      for (let src of song.sources) {
        try {
          src.start(this);
          loaded = true;
          break;
        } catch (e) {
          Logger.warn("Couldn't load source", src);
        }
      }
    }

    if (!loaded) {
      Logger.error("Couldn't load any sources");
    }
  }

  changePlayer(newPlayer?: Player) {
    if (this.player !== undefined) {
      this.player.destruct();
    }
    this.player = newPlayer;
  }

};

export default MusicManager;
