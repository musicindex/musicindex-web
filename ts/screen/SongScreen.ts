import App from "../App.js";
import Util from "../util/Util.js";
import AlbumScreen from "./AlbumScreen.js";
import ArtistScreen from "./ArtistScreen.js";
import Screen from "./Screen.js";

class SongScreen extends Screen {
  songId: string;
  
  override getClass() {
    return SongScreen;
  }

  static override getId() {
    return "song";
  }

  override async open() {
    let song = await App.i.songStorage.getItem(this.songId);

    this.seli("#name").innerText = song !== null ? song.title : this.songId;

    this.seli("#description").innerText = "";
    if (song.description !== undefined) {
      this.seli("#description").innerText = song.description;
    } else {
      let i = document.createElement("i");
      i.innerText = "No description provided";
      this.seli("#description").appendChild(i);
    }


    let btns = this.seli("#buttons");
    btns.innerText = "";

    let playBtn = document.createElement("button");
    playBtn.innerText = "Play";
    playBtn.addEventListener("click", () => {
      App.i.changeSong(this.songId);
    });
    btns.appendChild(playBtn);
    btns.appendChild(document.createElement("br"));
    btns.appendChild(await Util.generateDOM_artist(song.artists));
    btns.appendChild(document.createElement("br"));
    btns.appendChild(await Util.generateDOM_album(song.album));


    this.seli("#data").innerText = JSON.stringify(song);
  }

  override getParams() {
    return [this.songId];
  }

  static override fromParams(params: Array<string>): SongScreen {
    return new SongScreen(params[0]);
  }

  constructor(id: string) {
    super();
    this.songId = id;
  }
};

export default SongScreen;
