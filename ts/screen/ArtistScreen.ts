import App from "../App.js";
import Util from "../util/Util.js";
import Screen from "./Screen.js";

class ArtistScreen extends Screen {
  artistId: string;
  
  override getClass() {
    return ArtistScreen;
  }

  static override getId() {
    return "artist";
  }

  override async open() {
    let artist = await App.i.artistStorage.getItem(this.artistId);

    this.seli("#name").innerText = artist !== null ? artist.name : this.artistId;

    this.seli("#description").innerText = "";
    if (artist.description !== undefined) {
      this.seli("#description").innerText = artist.description;
    } else {
      let i = document.createElement("i");
      i.innerText = "No description provided";
      this.seli("#description").appendChild(i);
    }

    this.seli("#data").innerText = JSON.stringify(artist);


    // Clear old albums list
    let albumsContainer = this.seli("#albums");
    albumsContainer.innerText = "";

    // Load new albums
    for (let id of artist.albums) {
      albumsContainer.appendChild(await Util.generateDOM_albumTile(id));
    }


    // Clear old singles list
    let singlesContainer = this.seli("#singles");
    singlesContainer.innerText = "";

    // Load new singles
    for (let id of artist.songs) {
      singlesContainer.appendChild(await Util.generateDOM_songTile(id));
    }
  }

  override getParams() {
    return [this.artistId];
  }

  static override fromParams(params: Array<string>): ArtistScreen {
    return new ArtistScreen(params[0]);
  }

  constructor(id: string) {
    super();
    this.artistId = id;
  }
};

export default ArtistScreen;
