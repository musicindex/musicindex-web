abstract class Screen {

  // [TODO] change the proper type annotation
  abstract getClass(): any;

  static getId(): string {
    throw new Error("getId() method has to be overridden by subclass");
  }

  getId_i(): string {
    return this.getClass().getId();
  }

  static getScreenDivId() {
    return `screen-${this.getId()}`;
  }

  getScreenDivId_i() {
    return `screen-${this.getId_i()}`;
  }



  // Get root element of the screen
  static get e() {
    return document.getElementById(this.getScreenDivId());
  }

  get ei() {
    return document.getElementById(this.getScreenDivId_i());
  }

  // Alias to this.e.querySelector()
  static sel(selectors: string): HTMLElement | null {
    return <HTMLElement> this.e.querySelector(selectors);
  }

  seli(selectors: string): HTMLElement | null {
    return <HTMLElement> this.ei.querySelector(selectors);
  }

  async open() {};
  close() {};
  pause() {};
  unpause() {};
  
  getParams() {
    return Array<string>();
  }

  static fromParams(params: Array<string>): Screen {
    return undefined;
  }

  static init() {};

};

export default Screen;
