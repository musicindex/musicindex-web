import Config from "../Config.js";
import GETRequest from "../util/GETRequest.js";
import Logger from "../util/Logger.js";
import PUTRequest from "../util/PUTRequest.js";
import Screen from "./Screen.js";

class TestScreen extends Screen {

  override getClass() {
    return TestScreen;
  }

  static override getId() {
    return "test";
  }

  static override init(): void {
    let container = this.sel("#testing");
    container.innerText = "";

    let textarea = document.createElement("textarea");
    container.appendChild(textarea);

    let btn = document.createElement("button");
    btn.innerText = "PUT test";
    btn.addEventListener("click", async () => {
      let req = new PUTRequest(`${Config.apiUrl}/song`, textarea.value);
      await req.send();
      Logger.log(req.response);
    });
    container.appendChild(btn);

    let btn2 = document.createElement("button");
    btn2.innerText = "GET test";
    btn2.addEventListener("click", async () => {
      let req = new GETRequest(`${Config.apiUrl}/artists`);
      await req.send();
      Logger.log(req.response);
    });
    container.appendChild(btn2);
  }

  static override fromParams(params: string[]): Screen {
    return new TestScreen();
  }
};

export default TestScreen;
