import Config from "../Config.js";
import GETRequest from "../util/GETRequest.js";
import PUTRequest from "../util/PUTRequest.js";
import Util from "../util/Util.js";
import Screen from "./Screen.js";

class AddAlbumScreen extends Screen {
  
  override getClass() {
    return AddAlbumScreen;
  }

  static override getId() {
    return "addAlbum";
  }

  static async updateName() {
    let idInput = (<HTMLInputElement> this.sel("#id"));
    let name    = (<HTMLInputElement> this.sel("#name")).value;
    let id = Util.formatId(name);
    idInput.classList.remove("error");
    
    if (idInput.value !== id) {
      idInput.value = id;
    
      let data = await this.getExistence(id);
      if (data.exists) {
        idInput.value = data.nextFreeId;
      }
      if (!data.validId) {
        idInput.classList.add("error");
        return;
      }
    }
  }

  static async getExistence(id: string) {
    let req = new GETRequest(`${Config.apiUrl}/exists/album/${id}`);
    await req.send();
    return JSON.parse(req.response);
  }

  static async getData() {
    function getOr(check: string, value: any = check, nothing: any = undefined) {
      return check === "" ? nothing : value;
    }

    function toBSONDate(timestamp: number): object {
      return {
        $date: {
          $numberLong: `${timestamp}`
        }
      }
    }

    let id          = (<HTMLInputElement>    this.sel("#id"         )).value;
    let name        = (<HTMLInputElement>    this.sel("#name"       )).value;
    let description = (<HTMLTextAreaElement> this.sel("#description")).value;
    let artists     = (<HTMLInputElement>    this.sel("#artists"    )).value;
    let trackCount  = (<HTMLInputElement>    this.sel("#trackCount" )).value;
    let discCount   = (<HTMLInputElement>    this.sel("#discCount"  )).value;
    let genre       = (<HTMLInputElement>    this.sel("#genre"      )).value;
    let images      = (<HTMLTextAreaElement> this.sel("#images"     )).value;
    let releaseDate = (<HTMLInputElement>    this.sel("#releaseDate")).value;

    return {
      id:          id,
      name:        name,
      description: getOr(description),
      artists:     getOr(artists,     artists.split(",").map(x => x.trim()), []),
      trackCount:  getOr(trackCount,  parseInt(trackCount)),
      discCount:   getOr(discCount,   parseInt(discCount)),
      genre:       getOr(genre),
      images:      getOr(images,      images.split("\n").map(x => ({url: x.trim()})), []),
      releaseDate: getOr(releaseDate, toBSONDate(new Date(releaseDate).valueOf())),
      credits:     <any> [],
      links:       <any> [],
    };
  }

  static async updateDataPreview() {
    let dataTextarea = <HTMLTextAreaElement> this.sel("#data");
    dataTextarea.value = JSON.stringify(await this.getData());
  }

  static async submit() {
    await this.updateDataPreview();
    let req = new PUTRequest(`${Config.apiUrl}/album`, JSON.stringify(await this.getData()));
    let status = this.sel("#status");
    status.classList.remove("error");
    status.classList.remove("success");
    status.classList.remove("hidden");
    try {
      await req.send();
      status.innerText = req.response;
      status.classList.add("success");
    } catch (e) {
      status.innerText = `${e} ${req.response}`;
      status.classList.add("error");
    }
  }

  override async open() {
    let status = this.seli("#status");
    status.classList.add("hidden");
  }

  static override init(): void {
    this.sel("#name")  .addEventListener("keyup",  async () => { await AddAlbumScreen.updateName(); });
    this.sel("#name")  .addEventListener("change", async () => { await AddAlbumScreen.updateName(); });
    this.sel("#submit").addEventListener("click",  async () => { await AddAlbumScreen.submit();     });

    this.e.querySelectorAll("input").forEach(elem => {
      elem.addEventListener("change", async () => {
        await AddAlbumScreen.updateDataPreview();
      })
    })
  }

  static override fromParams(params: string[]): Screen {
    return new AddAlbumScreen();
  }
};

export default AddAlbumScreen;
