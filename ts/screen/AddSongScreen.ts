import Config from "../Config.js";
import GETRequest from "../util/GETRequest.js";
import PUTRequest from "../util/PUTRequest.js";
import Util from "../util/Util.js";
import Screen from "./Screen.js";

class AddSongScreen extends Screen {

  static readonly youTubeIdRegex = /((https?:\/\/)?(www\.)?((music\.)?youtube\.com\/watch\?v=)|(youtu\.be\/))?([A-Za-z0-9\-_]{11})[.\S]*/;
  
  override getClass() {
    return AddSongScreen;
  }

  static override getId() {
    return "addSong";
  }

  static lastYouTubeId = "";
  static getYouTubeId() {
    let input = <HTMLInputElement> this.sel("#url");
    let match = input.value.match(this.youTubeIdRegex);
    if (match === null) {
      input.classList.add("error");
    } else {
      input.classList.remove("error");
    }
    return match === null ? "" : match[7];
  }

  static updateEmbed() {
    let iframe = <HTMLIFrameElement> this.sel("#youtubeEmbed");
    let id = this.getYouTubeId();
    if (id !== this.lastYouTubeId) {
      this.lastYouTubeId = id;
      iframe.src = `https://www.youtube.com/embed/${id}`;
    }
  }

  static async updateTitle() {
    let idInput = (<HTMLInputElement> this.sel("#id"));
    let title   = (<HTMLInputElement> this.sel("#title")).value;
    let id = Util.formatId(title);
    idInput.classList.remove("error");
    
    if (idInput.value !== id) {
      idInput.value = id;
    
      let data = await this.getExistence(id);
      if (data.exists) {
        idInput.value = data.nextFreeId;
      }
      if (!data.validId) {
        idInput.classList.add("error");
        return;
      }
    }
  }

  static async updateId() {
    let idInput = (<HTMLInputElement> this.sel("#id"));
    idInput.classList.remove("error");

    let id = idInput.value;
    if (id != Util.formatId(id)) {
      idInput.classList.add("error");
      return;
    }

    let data = await this.getExistence(id);
    if (!data.validId) {
      idInput.classList.add("error");
      return;
    }
    if (data.exists) {
      idInput.classList.add("error");
    }
  }

  static async getExistence(id: string) {
    let req = new GETRequest(`${Config.apiUrl}/exists/song/${id}`);
    await req.send();
    return JSON.parse(req.response);
  }

  static async getData() {
    await this.updateId();

    function getOr(check: string, value: any = check, nothing: any = undefined) {
      return check === "" ? nothing : value;
    }

    function toBSONDate(timestamp: number): object {
      return {
        $date: {
          $numberLong: `${timestamp}`
        }
      }
    }

    let id          = (<HTMLInputElement>    this.sel("#id"         )).value;
    let title       = (<HTMLInputElement>    this.sel("#title"      )).value;
    let description = (<HTMLTextAreaElement> this.sel("#description")).value;
    let artists     = (<HTMLInputElement>    this.sel("#artists"    )).value;
    let album       = (<HTMLInputElement>    this.sel("#album"      )).value;
    let trackNumber = (<HTMLInputElement>    this.sel("#trackNumber")).value;
    let discNumber  = (<HTMLInputElement>    this.sel("#discNumber" )).value;
    let genre       = (<HTMLInputElement>    this.sel("#genre"      )).value;
    let duration    = (<HTMLInputElement>    this.sel("#duration"   )).value;
    let explicit    = (<HTMLInputElement>    this.sel("#explicit"   )).checked;
    let images      = (<HTMLTextAreaElement> this.sel("#images"     )).value;
    let releaseDate = (<HTMLInputElement>    this.sel("#releaseDate")).value;
    let license     = (<HTMLInputElement>    this.sel("#license"    )).value;

    return {
      id:          id,
      title:       title,
      description: getOr(description),
      artists:     getOr(artists,     artists.split(",").map(x => x.trim()), []),
      album:       getOr(album),
      trackNumber: getOr(trackNumber, parseInt(trackNumber)),
      discNumber:  getOr(discNumber,  parseInt(discNumber)),
      genre:       getOr(genre),
      duration:    getOr(duration,    parseInt(duration)),
      explicit:    explicit,
      images:      getOr(images,      images.split("\n").map(x => ({url: x.trim()})), []),
      releaseDate: getOr(releaseDate, toBSONDate(new Date(releaseDate).valueOf())),
      credits:     <any> [],
      license:     getOr(license),
      sources: [
        {
          type: "youtube",
          id: this.getYouTubeId()
        }
      ],
      timingMap:   <any> [],
      lyrics:      <any> [],
      parents:     <any> [],
    };
  }

  static async updateDataPreview() {
    let dataTextarea = <HTMLTextAreaElement> this.sel("#data");
    dataTextarea.value = JSON.stringify(await this.getData());
  }

  static async submit() {
    await this.updateDataPreview();
    let req = new PUTRequest(`${Config.apiUrl}/song`, JSON.stringify(await this.getData()));
    let status = this.sel("#status");
    status.classList.remove("error");
    status.classList.remove("success");
    status.classList.remove("hidden");
    try {
      await req.send();
      status.innerText = req.response;
      status.classList.add("success");
    } catch (e) {
      status.innerText = `${e} ${req.response}`;
      status.classList.add("error");
    }
  }

  override async open() {
    AddSongScreen.updateEmbed();
    let status = this.seli("#status");
    status.classList.add("hidden");
  }

  static override init(): void {
    this.sel("#url")   .addEventListener("keyup",  () => { AddSongScreen.updateEmbed(); });
    this.sel("#url")   .addEventListener("change", () => { AddSongScreen.updateEmbed(); });
    this.sel("#id")    .addEventListener("keyup",  async () => { await AddSongScreen.updateId();    });
    this.sel("#id")    .addEventListener("change", async () => { await AddSongScreen.updateId();    });
    this.sel("#title") .addEventListener("keyup",  async () => { await AddSongScreen.updateTitle(); });
    this.sel("#title") .addEventListener("change", async () => { await AddSongScreen.updateTitle(); });
    this.sel("#submit").addEventListener("click",  async () => { await AddSongScreen.submit();      });

    this.e.querySelectorAll("input").forEach(elem => {
      elem.addEventListener("change", async () => {
        await AddSongScreen.updateDataPreview();
      })
    })

    let urlInput = <HTMLInputElement> this.sel("#url");
    urlInput.addEventListener("click", () => {
      setTimeout(() => {
        urlInput.select();
        urlInput.setSelectionRange(0, urlInput.value.length);
      }, 1)
    });
  }

  static override fromParams(params: string[]): Screen {
    return new AddSongScreen();
  }
};

export default AddSongScreen;
