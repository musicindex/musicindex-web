import App from "../App.js";
import Config from "../Config.js";
import GETRequest from "../util/GETRequest.js";
import Logger from "../util/Logger.js";
import Util from "../util/Util.js";
import Screen from "./Screen.js";

class SearchScreen extends Screen {

  override getClass() {
    return SearchScreen;
  }

  static override getId() {
    return "search";
  }

  static query = "";
  static filter = "";

  // Submit button callback
  static async search() {
    let searchField = <HTMLInputElement>this.sel("#search");
    let filterField = <HTMLOptionElement>this.sel("#filter");
    let query = searchField.value;
    let filter = filterField.value;
    if (query === this.query && filter === this.filter) return;
    this.query = query;
    this.filter = filter;

    // Clear old results
    let container = this.sel("#results");
    container.innerText = "";

    try {
      let req = new GETRequest(`${Config.apiUrl}/search/${query}`);
      await req.send();
      let res = req.response;
      let results = JSON.parse(res);
      App.i.updateHashManually(false);
      Logger.log(`Search ${query}:`, results);

      // Load new results
      for (let result of results.items) {
        let type = result.type;

        // Filter out results
        if (filter !== "none" && filter !== type) continue;

        let id = result.id;
        if (type === "song") {
          container.appendChild(await Util.generateDOM_songTile(id));
        } else if (type === "artist") {
          container.appendChild(await Util.generateDOM_artistTile(id));
        } else if (type === "album") {
          container.appendChild(await Util.generateDOM_albumTile(id));
        } else {
          let tile = document.createElement("div");
          tile.classList.add("item-tile");
          tile.classList.add("generic-tile");
          tile.innerText = JSON.stringify(result);
          container.appendChild(tile);
        }
      }
    } catch (e) { }
  }

  override async open() {
    await SearchScreen.search();
  }

  override getParams() {
    return [SearchScreen.query, SearchScreen.filter];
  }

  static override fromParams(params: string[]): Screen {
    return new SearchScreen(params[0], params[1]);
  }

  static override init(): void {
    this.sel("#search").addEventListener("keyup", async () => { await SearchScreen.search(); });
    this.sel("#submit").addEventListener("click", async () => { await SearchScreen.search(); });
    this.sel("#filter").addEventListener("change", async () => { await SearchScreen.search(); });
  }

  constructor(query?: string, filter?: string) {
    super();
    if (query !== undefined) {
      SearchScreen.query = query;
      (<HTMLInputElement>this.seli("#search")).value = query;
    }
    if (filter !== undefined && filter !== "") {
      SearchScreen.filter = filter;
      (<HTMLSelectElement>this.seli("#filter")).value = filter;
    }
  }
};

export default SearchScreen;
