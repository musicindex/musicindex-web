import App from "../App.js";
import Album from "../data/Album.js";
import Screen from "./Screen.js";
import Util from "../util/Util.js";

class PlaylistScreen extends Screen {
  playlistId: string;
  
  override getClass() {
    return PlaylistScreen;
  }

  static override getId() {
    return "playlist";
  }

  override async open() {
    let playlist = await App.i.playlistStorage.getItem(this.playlistId);

    let playlistContainer = document.querySelector("#screen-playlist #list");
    let tbody = playlistContainer.querySelector("tbody");
    tbody.innerHTML = "";

    {
      let tr = document.createElement("tr");
      tr.innerHTML = `
        <th>#</th>
        <th>Title</th>
        <th>Artist</th>
        <th>Album</th>
        <th>Year</th>
      `;
      tbody.appendChild(tr);
    }

    if (playlist !== undefined) {
      for (let i = 0; i < playlist.songs.length; i++) {
        let songId = playlist.songs[i];
        let song = await App.i.songStorage.getItem(songId);
        let album: Album = null;
        if (song !== null && song.album !== undefined) {
          album = await App.i.albumStorage.getItem(song.album);
        }

        let tr = document.createElement("tr");
        tr.classList.add("song-row");
        tr.addEventListener("click", () => {
          App.i.deselectItem();
          tr.classList.add("item-selected");
        });
        tr.addEventListener("dblclick", async () => {
          await App.i.playSongInCurrentPlaylist(i);
        });

        {
          let td = document.createElement("td");
          td.innerText = `${i + 1}`;
          tr.appendChild(td);
        }
        {
          let td = document.createElement("td");
          td.appendChild(await Util.generateDOM_title(songId, song === null ? undefined : song.title));
          tr.appendChild(td);
        }
        {
          let td = document.createElement("td");
          td.appendChild(await Util.generateDOM_artist(song === null ? undefined : song.artists));
          tr.appendChild(td);
        }
        {
          let td = document.createElement("td");
          td.appendChild(await Util.generateDOM_album(song === null ? undefined : song.album));
          tr.appendChild(td);
        }
        {
          let td = document.createElement("td");
          let date = null;

          if (song !== null) {
            if (song.releaseDate !== undefined) {
              date = new Date(song.releaseDate);
            } else {
              if (album !== null && album.releaseDate !== undefined) {
                date = new Date(album.releaseDate);
              }
            }
          }

          if (date !== null) {
            td.innerText = `${date}`;
          }

          tr.appendChild(td);
        }

        tbody.appendChild(tr);
      }
    }

    App.i.currentPlaylist = this.playlistId;
  }

  override getParams() {
    return [this.playlistId];
  }

  static override fromParams(params: Array<string>): PlaylistScreen {
    return new PlaylistScreen(params[0]);
  }

  constructor(id: string) {
    super();
    this.playlistId = id;
  }
};

export default PlaylistScreen;
