import Config from "../Config.js";
import GETRequest from "../util/GETRequest.js";
import PUTRequest from "../util/PUTRequest.js";
import Util from "../util/Util.js";
import Screen from "./Screen.js";

class AddArtistScreen extends Screen {
  
  override getClass() {
    return AddArtistScreen;
  }

  static override getId() {
    return "addArtist";
  }

  static async updateName() {
    let idInput = (<HTMLInputElement> this.sel("#id"));
    let name    = (<HTMLInputElement> this.sel("#name")).value;
    let id = Util.formatId(name);
    idInput.classList.remove("error");
    
    if (idInput.value !== id) {
      idInput.value = id;
    
      let data = await this.getExistence(id);
      if (data.exists) {
        idInput.value = data.nextFreeId;
      }
      if (!data.validId) {
        idInput.classList.add("error");
        return;
      }
    }
  }

  static async getExistence(id: string) {
    let req = new GETRequest(`${Config.apiUrl}/exists/artist/${id}`);
    await req.send();
    return JSON.parse(req.response);
  }

  static async getData() {
    function getOr(check: string, value: any = check, nothing: any = undefined) {
      return check === "" ? nothing : value;
    }

    let id          = (<HTMLInputElement>    this.sel("#id"         )).value;
    let name        = (<HTMLInputElement>    this.sel("#name"       )).value;
    let description = (<HTMLTextAreaElement> this.sel("#description")).value;
    let image       = (<HTMLInputElement>    this.sel("#image"      )).value;

    return {
      id:          id,
      name:        name,
      description: getOr(description),
      image:       getOr(image),
      links:       <any> [],
    };
  }

  static async updateDataPreview() {
    let dataTextarea = <HTMLTextAreaElement> this.sel("#data");
    dataTextarea.value = JSON.stringify(await this.getData());
  }

  static async submit() {
    await this.updateDataPreview();
    let req = new PUTRequest(`${Config.apiUrl}/artist`, JSON.stringify(await this.getData()));
    let status = this.sel("#status");
    status.classList.remove("error");
    status.classList.remove("success");
    status.classList.remove("hidden");
    try {
      await req.send();
      status.innerText = req.response;
      status.classList.add("success");
    } catch (e) {
      status.innerText = `${e} ${req.response}`;
      status.classList.add("error");
    }
  }

  override async open() {
    let status = this.seli("#status");
    status.classList.add("hidden");
  }

  static override init(): void {
    this.sel("#name")  .addEventListener("keyup",  async () => { await AddArtistScreen.updateName(); });
    this.sel("#name")  .addEventListener("change", async () => { await AddArtistScreen.updateName(); });
    this.sel("#submit").addEventListener("click",  async () => { await AddArtistScreen.submit();     });

    this.e.querySelectorAll("input").forEach(elem => {
      elem.addEventListener("change", async () => {
        await AddArtistScreen.updateDataPreview();
      })
    })
  }

  static override fromParams(params: string[]): Screen {
    return new AddArtistScreen();
  }
};

export default AddArtistScreen;
