import Screen from "./Screen.js";

class YouTubeScreen extends Screen {
  
  override getClass() {
    return YouTubeScreen;
  }

  static override getId() {
    return "youtube";
  }

  static override fromParams(params: string[]): Screen {
    return new YouTubeScreen();
  }
};

export default YouTubeScreen;
