import AddArtistScreen from "./AddArtistScreen.js";
import AddSongScreen from "./AddSongScreen.js";
import AddAlbumScreen from "./AddAlbumScreen.js";
import AlbumScreen from "./AlbumScreen.js";
import ArtistScreen from "./ArtistScreen.js";
import HomeScreen from "./HomeScreen.js";
import PlaylistScreen from "./PlaylistScreen.js";
import SearchScreen from "./SearchScreen.js";
import SongScreen from "./SongScreen.js";
import TestScreen from "./TestScreen.js";
import YouTubeScreen from "./YouTubeScreen.js";

abstract class ScreenFactory {

  static readonly allScreens: { [index: string]: any } = {
    home: HomeScreen,
    test: TestScreen,
    youtube: YouTubeScreen,
    search: SearchScreen,
    playlist: PlaylistScreen,
    song: SongScreen,
    artist: ArtistScreen,
    album: AlbumScreen,
    addSong: AddSongScreen,
    addArtist: AddArtistScreen,
    addAlbum: AddAlbumScreen,
  }

  static fromParams(params: Array<string>) {
    let id = params[0];
    let rest = params.slice(1);

    let screen = <any> this.allScreens[id];
    if (screen !== undefined) {
      return screen.fromParams(rest);
    } else {
      return undefined;
    }
  }

  static init() {
    for (let screen of Object.values(this.allScreens)) {
      screen.init();
    }
  }
};

export default ScreenFactory;
