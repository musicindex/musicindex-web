import App from "../App.js";
import Util from "../util/Util.js";
import ArtistScreen from "./ArtistScreen.js";
import Screen from "./Screen.js";

class AlbumScreen extends Screen {
  albumId: string;
  
  override getClass() {
    return AlbumScreen;
  }

  static override getId() {
    return "album";
  }

  override async open() {
    let album = await App.i.albumStorage.getItem(this.albumId);

    this.seli("#name").innerText = album !== null ? album.name : this.albumId;

    this.seli("#description").innerText = "";
    if (album.description !== undefined) {
      this.seli("#description").innerText = album.description;
    } else {
      let i = document.createElement("i");
      i.innerText = "No description provided";
      this.seli("#description").appendChild(i);
    }


    let btns = this.seli("#buttons");
    btns.innerText = "";
    btns.appendChild(await Util.generateDOM_artist(album.artists));

    this.seli("#data").innerText = JSON.stringify(album);


    // [TODO] change songtiles to playlist-like table

    // Clear old songs list
    let songContainer = this.seli("#songs");
    songContainer.innerText = "";

    // Load new songs
    for (let id of album.songs) {
      songContainer.appendChild(await Util.generateDOM_songTile(id));
    }
  }

  override getParams() {
    return [this.albumId];
  }

  static override fromParams(params: Array<string>): AlbumScreen {
    return new AlbumScreen(params[0]);
  }

  constructor(id: string) {
    super();
    this.albumId = id;
  }
};

export default AlbumScreen;
