import Config from "../Config.js";
import GETRequest from "../util/GETRequest.js";
import Util from "../util/Util.js";
import Screen from "./Screen.js";

class HomeScreen extends Screen {

  override getClass() {
    return HomeScreen;
  }

  static override getId() {
    return "home";
  }

  static async randomize() {
    const songCount = 20;
    const artistCount = 10;
    const albumCount = 10;

    async function generate(container: HTMLElement, type: string, generatorFunction: Function, maxCount: number) {
      container.innerText = "";
      try {
        let req = new GETRequest(`${Config.apiUrl}/random/${type}/${maxCount}`);
        await req.send();
        let ids = JSON.parse(req.response);

        for (let i = 0; i < Math.min(ids.length, maxCount); i++) {
          let id = ids[i];
          container.appendChild(await generatorFunction(id));
        }
      } catch (e) {
        container.appendChild(document.createTextNode(`Error: ${e}`));
      }
    }

    generate(this.sel(`#randomSongs`), "song", Util.generateDOM_songTile, songCount);
    generate(this.sel(`#randomArtists`), "artist", Util.generateDOM_artistTile, artistCount);
    generate(this.sel(`#randomAlbums`), "album", Util.generateDOM_albumTile, albumCount);
  }

  static override async init() {
    await this.randomize();
    this.sel("#randomize").addEventListener("click", async () => {
      await HomeScreen.randomize();
    })
  }

  static override fromParams(params: string[]): Screen {
    return new HomeScreen();
  }
};

export default HomeScreen;
