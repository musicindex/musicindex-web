enum LinkType {
  WEBSITE = "website",
  YOUTUBE_VIDEO = "youtube_video",
  YOUTUBE_CHANNEL = "youtube_channel",
  YOUTUBE_PLAYLIST = "youtube_playlist",
}

class Link {
  type: LinkType | string;
  url: string;
  label?: string;

  constructor(type: LinkType | string = LinkType.WEBSITE, url = "", label?: string) {
    this.type = type;
    this.url = url;
    this.label = label;
  }

  static loadData(data: any): Link | undefined {
    // [TODO] use Util methods for loading
    if (typeof data !== "object") return;

    let result = new Link();

    if (typeof data.type === "string") {
      result.type = data.type;
    }

    if (typeof data.url === "string") {
      result.url = data.url;
    }

    if (typeof data.label === "string") {
      result.label = data.label;
    }

    return result;
  }
}

export default Link;
export { LinkType };
