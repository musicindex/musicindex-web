import Util from "../util/Util.js";

class TimingEvent {
  time: number;
  bpm?: number;
  timeSignature?: [number, number];
  key?: string;

  static loadData(data: any): TimingEvent | undefined {
    if (typeof data !== "object") return;
    let result = new TimingEvent();

    Util.loadField(result, data, "time", "number");
    Util.loadField(result, data, "bpm", "number");
    if (Array.isArray(result.timeSignature)) {
      result.timeSignature = [
        Util.tryInto(data.timeSignature[0], "number"),
        Util.tryInto(data.timeSignature[1], "number")
      ];
    }
    Util.loadField(result, data, "key", "string");

    return result;
  }
}

export default TimingEvent;
