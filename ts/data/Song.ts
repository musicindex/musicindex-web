import Credit from "./Credit.js";
import ImageInfo from "./ImageInfo.js";
import Link from "./Link.js";
import Source from "./sources/Source.js";
import SourceFactory from "./sources/SourceFactory.js";
import TimingEvent from "./TimingEvent.js";
import Util from "../util/Util.js"

class Song {
  title: string;
  description?: string;
  artists: string[]; // Artist IDs
  artistString?: string;
  album?: string; // Album ID
  genre?: string;
  duration?: number;
  releaseDate?: number;
  explicit: boolean;
  images: ImageInfo[];
  credits: Credit[];
  links: Link[];
  license?: string;
  sources: Source[];
  // timingMap: TimingEvent[];
  lyrics?: string;
  parents: string[];
  addDate?: number;
  updateDate?: number;

  static loadData(data: any): Song | undefined {
    if (typeof data !== "object") return;

    let songData = data.song;
    if (typeof songData !== "object") return;

    let result = new Song();

    Util.loadField(result, songData, "title", "string");
    Util.loadField(result, songData, "description", "string");
    Util.loadArray(result, songData, "artists", "string");
    Util.loadField(result, songData, "artistString", "string");
    Util.loadField(result, songData, "album", "string");
    Util.loadField(result, songData, "genre", "string");
    Util.loadField(result, songData, "duration", "number");
    Util.loadField(result, songData, "releaseDate", "number");
    Util.loadField(result, songData, "explicit", "boolean");
    Util.loadArray(result, songData, "images", ImageInfo);
    Util.loadArray(result, songData, "credits", Credit);
    Util.loadField(result, songData, "license", "string");
    Util.loadArray(result, songData, "sources", SourceFactory);
    // Util.loadArray(result, songData, "timingMap", TimingEvent);
    Util.loadField(result, songData, "lyrics", "string");
    Util.loadArray(result, songData, "parents", "string");
    Util.loadField(result, songData, "addDate", "number");
    Util.loadField(result, songData, "updateDate", "number");

    return result;
  }
}

export default Song;
