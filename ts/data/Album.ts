import ImageInfo from "./ImageInfo.js";
import Credit from "./Credit.js";
import Link from "./Link.js";
import Util from "../util/Util.js"

class Album {
  name: string;
  description?: string;
  artists: string[]; // Artist IDs
  trackCount?: number;
  discCount?: number;
  genre?: string;
  images: ImageInfo[];
  releaseDate?: number;
  credits: Credit[];
  links: Link[];
  addDate?: number;
  updateDate?: number;

  songs: string[]; // Song IDs

  static loadData(data: any): Album | undefined {
    if (typeof data !== "object") return;

    let albumData = data.album;
    if (typeof albumData !== "object") return;

    let result = new Album();

    Util.loadField(result, albumData, "name", "string");
    Util.loadField(result, albumData, "description", "string");
    Util.loadArray(result, albumData, "artists", "string");
    Util.loadField(result, albumData, "trackCount", "number");
    Util.loadField(result, albumData, "discCount", "number");
    Util.loadField(result, albumData, "genre", "string");
    Util.loadArray(result, albumData, "images", ImageInfo);
    Util.loadField(result, albumData, "releaseDate", "number");
    Util.loadArray(result, albumData, "credits", Credit);
    Util.loadArray(result, albumData, "links", Link);
    Util.loadField(result, albumData, "addDate", "number");
    Util.loadArray(result, data, "songs", "string");

    return result;
  }
}

export default Album;
