import MusicManager from "../../music/MusicManager.js";
import URLPlayer from "../../music/players/URLPlayer.js";
import Logger from "../../util/Logger.js";
import Source from "./Source.js";

class URLSource extends Source {
  url: string;

  static loadData(data: any): URLSource | undefined {
    if (typeof data !== "object") return;

    let result = new URLSource();

    if (typeof data.url === "string") {
      result.url = data.url;
    }

    return result;
  }

  start(m: MusicManager): void {
    Logger.log("Loading URLSource: ", this);
    m.changePlayer(new URLPlayer(this.url));
    m.player.setVolume(m.volume);
  }
};

export default URLSource;
