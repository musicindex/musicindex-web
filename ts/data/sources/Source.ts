import MusicManager from "../../music/MusicManager.js";

enum SourceType {
  URL = "url",
  YOUTUBE = "youtube"
}

abstract class Source {
  abstract start(m: MusicManager): void;
}

export default Source;
export { SourceType };
