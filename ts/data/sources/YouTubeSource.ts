import MusicManager from "../../music/MusicManager.js";
import YouTubePlayer from "../../music/players/YouTubePlayer.js";
import Logger from "../../util/Logger.js";
import Source from "./Source.js";

class YouTubeSource extends Source {
  id: string;

  static loadData(data: any): YouTubeSource | undefined {
    if (typeof data !== "object") return;

    let result = new YouTubeSource();

    if (typeof data.id === "string") {
      result.id = data.id;
    }

    return result;
  }

  start(m: MusicManager): void {
    Logger.log("Loading YouTubeSource: ", this);
    m.changePlayer(new YouTubePlayer(this.id));
    m.player.setVolume(m.volume);
  }
};

export default YouTubeSource;
