import Source, { SourceType } from "./Source.js";
import URLSource from "./URLSource.js";
import YouTubeSource from "./YouTubeSource.js";

abstract class SourceFactory {
  static loadData(data: any): Source | undefined {
    if (typeof data !== "object") return;
    if (typeof data.type !== "string") return;

    switch (data.type) {
      case SourceType.URL:
        return URLSource.loadData(data);
      case SourceType.YOUTUBE:
        return YouTubeSource.loadData(data);
    }
  }
}

export default SourceFactory;
