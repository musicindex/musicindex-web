class Playlist {
  name: string;
  owner: string; // User ID of the playlist creator
  description?: string;
  songs: Array<string>; // Song IDs

  constructor(name: string, owner: string, description?: string, songs?: Array<string>) {
    this.name = name;
    this.owner = owner;
    this.description = description;
    if (songs === undefined) {
      this.songs = new Array<string>();
    } else {
      this.songs = songs;
    }
  }
}

export default Playlist;
