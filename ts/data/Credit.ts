import Util from "../util/Util.js";

class Credit {
  role?: string;
  credit?: string;
  name?: string;

  static loadData(data: any): Credit | undefined {
    if (typeof data !== "object") return;
    let result = new Credit();

    Util.loadField(result, data, "role", "string");
    Util.loadField(result, data, "credit", "string");
    Util.loadField(result, data, "name", "string");

    return result;
  }
}

export default Credit;
