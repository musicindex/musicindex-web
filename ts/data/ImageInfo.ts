import Util from "../util/Util.js";

class ImageInfo {
  url: string;
  label?: string;

  static loadData(data: any): ImageInfo | undefined {
    if (typeof data !== "object") return;
    let result = new ImageInfo();

    Util.loadField(result, data, "url", "string");
    Util.loadField(result, data, "label", "string");

    return result;
  }
}

export default ImageInfo;
