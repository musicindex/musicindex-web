import Link from "./Link.js";
import Util from "../util/Util.js"

class Artist {
  name?: string;
  description?: string;
  image?: string;
  links: Link[];
  addDate?: number;
  updateDate?: number;

  songs: string[]; // Song IDs
  albums: string[]; // Album IDs

  static loadData(data: any): Artist | undefined {
    if (typeof data !== "object") return;

    let artistData = data.artist;
    if (typeof artistData !== "object") return;

    let result = new Artist();

    Util.loadField(result, artistData, "name", "string");
    Util.loadField(result, artistData, "description", "string");
    Util.loadField(result, artistData, "image", "string");
    Util.loadArray(result, artistData, "links", Link);
    Util.loadField(result, artistData, "addDate", "number");
    Util.loadField(result, artistData, "updateDate", "number");
    Util.loadArray(result, data, "songs", "string");
    Util.loadArray(result, data, "albums", "string");

    return result;
  }
}

export default Artist;
