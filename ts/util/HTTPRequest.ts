class HTTPRequest {

  readonly method: string;
  url: string;
  body: string;
  response: string;
  status?: number;

  async send(expectedCode?: number | number[]) {
    let expectedCodes: number[];
    if (expectedCode === undefined) expectedCodes = [200];
    if (typeof expectedCode === "number") expectedCodes = [expectedCode];
    if (Array.isArray(expectedCode)) expectedCodes = expectedCode;

    let request = new XMLHttpRequest();
    request.open(this.method, this.url, true);
    return new Promise((resolve, reject) => {
      request.onload = e => {
        this.status = request.status;
        this.response = request.responseText;
        if (!expectedCodes.includes(this.status)) {
          reject(`Status code was ${this.status}, not 200`);
        }
        resolve(this.response);
      }
      request.onerror = e => {
        reject(e);
      }
      request.send(this.body);
    });
  }

  constructor(method: string, url: string, body: string) {
    this.method = method;
    this.url = url;
    this.body = body;
  }

};

export default HTTPRequest;
