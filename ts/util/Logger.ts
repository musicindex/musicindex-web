export default class Logger {
  static log(...args: any[]) {
    console.log("[Musiful]", ...args);
  }
  static warn(...args: any[]) {
    console.warn("[Musiful]", ...args);
  }
  static error(...args: any[]) {
    console.error("[Musiful]", ...args);
  }
  static trace(...args: any[]) {
    console.trace("[Musiful]", ...args);
  }

  private constructor() {

  }
}