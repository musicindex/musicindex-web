import HTTPRequest from "./HTTPRequest.js";

class PUTRequest extends HTTPRequest {

  constructor(url: string, body: string) {
    super("PUT", url, body);
  }

};

export default PUTRequest;
