import App from "../App.js";
import Album from "../data/Album.js";
import AlbumScreen from "../screen/AlbumScreen.js";
import ArtistScreen from "../screen/ArtistScreen.js";
import Logger from "./Logger.js";

export namespace Util {
  export function formatTime(seconds: number) {
    let secondsOnly = Math.floor(seconds % 60);
    let minutes = Math.floor(seconds / 60);

    let secondsString = secondsOnly < 10 ? ("0" + secondsOnly) : secondsOnly;
    return `${minutes}:${secondsString}`;
  }

  export function formatId(id: string) {
    return id.toLowerCase().replaceAll(/\W+/g, "_").replaceAll(/^_|_$/g, "");
  }



  export function tryInto(value: any, type: string): any {
    if (typeof value === type) {
      return value;
    }
    return undefined;
  }

  export interface Loadable {
    loadData(data: any): any;
  }

  export function loadField(result: any, data: any, key: string, type: string | Loadable) {
    if (typeof type === "string") {
      // You can use a string for type checking
      if (typeof data[key] === type) {
        result[key] = data[key];
      }
    } else {
      // ...or a class for loading whole objects
      type.loadData(data[key]);
    }
  }

  export function loadArray(result: any, data: any, key: string, type: string | Loadable) {
    result[key] = [];
    if (Array.isArray(data[key])) {
      for (let element of data[key]) {
        if (typeof type === "string") {
          // You can use a string for type checking
          if (typeof element === type) {
            result[key].push(element);
          }
        } else {
          // ...or a custom loading function
          result[key].push(type.loadData(element));
        }
      }
    }
  }



  export function generateDOM_invalidId(id?: string) {
    let result = document.createElement("i");
    if (id !== undefined) {
      result.classList.add("tooltip");
      result.title = `ID: ${id}`;
    }
    result.innerText = "Invalid";
    return result;
  }



  export function generateDOM_title(id: string, title?: string) {
    let result = document.createElement("span");

    if (title !== undefined) {
      let a = document.createElement("a");
      a.href = `#song#${encodeURIComponent(id)}`;
      a.innerText = `${title}`;
      result.appendChild(a);
    } else {
      result.appendChild(Util.generateDOM_invalidId(id));
    }

    return result;
  }

  export async function generateDOM_artist(ids?: string[]) {
    let result = document.createElement("span");

    if (ids === undefined) {
      result.appendChild(Util.generateDOM_invalidId());
    } else {
      for (let i = 0; i < ids.length; i++) {
        if (i > 0) {
          result.appendChild(document.createTextNode(", "));
        }

        let artistId = ids[i];
        let artist = await App.i.artistStorage.getItem(artistId);
        if (artist !== null) {
          let a = document.createElement("a");
          a.href = `#artist#${encodeURIComponent(artistId)}`;
          a.innerText = `${artist.name}`;
          result.appendChild(a);
        } else {
          result.appendChild(Util.generateDOM_invalidId(artistId));
        }
      }
    }

    return result;
  }

  export async function generateDOM_album(id?: string) {
    let result = document.createElement("span");

    if (id !== undefined) {
      let album = await App.i.albumStorage.getItem(id);
      if (album !== null) {
        let a = document.createElement("a");
        a.href = `#album#${encodeURIComponent(id)}`;
        a.innerText = `${album.name}`;
        result.appendChild(a);
      } else {
        result.appendChild(Util.generateDOM_invalidId(id));
      }
    }

    return result;
  }



  export async function generateDOM_songTile(id: string) {
    let song = await App.i.songStorage.getItem(id);
    let album: Album = null;
    if (song !== null && song.album !== undefined) {
      album = await App.i.albumStorage.getItem(song.album);
    }

    let div = document.createElement("div");
    div.classList.add("song-tile");
    div.classList.add("item-tile");
    div.addEventListener("click", () => {
      App.i.deselectItem();
      div.classList.add("item-selected");
    });
    div.addEventListener("dblclick", async () => {
      await App.i.changeSong(id);
      div.classList.add("item-playing");
    });

    let info = document.createElement("div");
    info.classList.add("stack");
    {
      {
        let title = document.createElement("span");
        title.classList.add("title");
        title.appendChild(Util.generateDOM_title(id, song === null ? undefined : song.title));
        info.appendChild(title);
      }
      {
        let artist = document.createElement("span");
        artist.classList.add("artist");
        artist.appendChild(await Util.generateDOM_artist(song === null ? undefined : song.artists));
        info.appendChild(artist);
      }
      let albumAndYear = document.createElement("span");
      {
        if (album !== null) {
          let album = document.createElement("span");
          album.classList.add("album");
          album.appendChild(await Util.generateDOM_album(song === null ? undefined : song.album));
          albumAndYear.appendChild(album);
        }
        {
          let span = document.createElement("span");
          let date = null;
          if (song !== null) {
            if (song.releaseDate !== undefined) {
              date = new Date(song.releaseDate);
            } else {
              if (album !== null && album.releaseDate !== undefined) {
                date = new Date(album.releaseDate);
              }
            }
          }

          if (date !== null) {
            if (album !== null) {
              albumAndYear.appendChild(document.createTextNode(" • "))
            }
            span.innerText = "" + date.getUTCFullYear();
            albumAndYear.appendChild(span);
          }
        }
      }
      info.appendChild(albumAndYear);
    }
    div.appendChild(info);

    return div;
  }

  export async function generateDOM_artistTile(id: string) {
    let artist = await App.i.artistStorage.getItem(id);

    let div = document.createElement("div");
    div.classList.add("artist-tile");
    div.classList.add("item-tile");
    div.addEventListener("click", () => {
      App.i.deselectItem();
      div.classList.add("item-selected");
    });
    div.addEventListener("dblclick", async () => {
      await App.i.pushScreen(new ArtistScreen(id));
    });

    let cover = document.createElement("img");
    cover.classList.add("profile-picture");
    cover.src = artist.image || "/res/img/user.svg";
    cover.addEventListener("click", async () => {
      await App.i.pushScreen(new ArtistScreen(id));
    });
    div.appendChild(cover);

    let info = document.createElement("div");
    info.classList.add("stack");
    {
      {
        let name = document.createElement("span");
        name.classList.add("name");
        name.appendChild(await Util.generateDOM_artist([id]));
        info.appendChild(name);
      }
      {
        let label = document.createElement("span");
        label.classList.add("label");
        label.innerText = "Artist";
        info.appendChild(label);
      }
    }
    div.appendChild(info);

    return div;
  }

  export async function generateDOM_albumTile(id: string) {
    let album = await App.i.albumStorage.getItem(id);

    let div = document.createElement("div");
    div.classList.add("album-tile");
    div.classList.add("item-tile");
    div.addEventListener("click", () => {
      App.i.deselectItem();
      div.classList.add("item-selected");
    });
    div.addEventListener("dblclick", async () => {
      await App.i.pushScreen(new AlbumScreen(id));
    });

    let cover = document.createElement("img");
    cover.classList.add("album-cover");
    cover.src = album.images[0] && album.images[0].url || "/res/img/broken_image.svg";
    cover.addEventListener("click", async () => {
      await App.i.pushScreen(new AlbumScreen(id));
    });
    div.appendChild(cover);

    let info = document.createElement("div");
    info.classList.add("stack");
    {
      {
        let name = document.createElement("span");
        name.classList.add("name");
        name.appendChild(await Util.generateDOM_album(id));
        info.appendChild(name);
      }
      {
        let label = document.createElement("span");
        label.classList.add("label");
        label.innerText = "Album";
        info.appendChild(label);
      }
      {
        let artistAndYear = document.createElement("span");

        let artist = document.createElement("span");
        artist.classList.add("artist");
        artist.appendChild(await Util.generateDOM_artist(album === null ? undefined : album.artists));
        artistAndYear.appendChild(artist);

        let span = document.createElement("span");
        let date = null;
        if (album !== null) {
          if (album.releaseDate !== undefined) {
            date = new Date(album.releaseDate);
          } else {
            if (album !== null && album.releaseDate !== undefined) {
              date = new Date(album.releaseDate);
            }
          }
        }

        if (date !== null) {
          artistAndYear.appendChild(document.createTextNode(" • "))
          span.innerText = "" + date.getUTCFullYear();
          artistAndYear.appendChild(span);
        }

        info.appendChild(artistAndYear);
      }
    }
    div.appendChild(info);

    return div;
  }
};

export default Util;
