import Config from "../Config.js";
import Artist from "../data/Artist.js";
import GETRequest from "../util/GETRequest.js";
import Logger from "../util/Logger.js";
import Storage from "./Storage.js";

class ArtistStorage extends Storage<string, Artist> {

  all = new Array<string>();

  override async fetchItem(id: string): Promise<Artist> {
    try {
      let req = new GETRequest(`${Config.apiUrl}/artist/${encodeURIComponent(id)}`);
      await req.send();
      let data = JSON.parse(req.response);
      return Artist.loadData(data);
    } catch (e) {
      return null;
    }
  }

  async fetchAll() {
    // Get all artist ids
    try {
      let req = new GETRequest(`${Config.apiUrl}/artists`);
      await req.send();
      let data = JSON.parse(req.response);

      let all = [];
      for (let x of data.items) {
        if (typeof x === "string") {
          all.push(x);
        }
      }
      this.all = all;
    } catch (e) {
      Logger.warn("Couldn't get artist list. Reason:", e)
    }
  }

  constructor() {
    super();
    let that = this;
    setTimeout(async () => {
      await that.fetchAll();
    })
  }
}

export default ArtistStorage;
