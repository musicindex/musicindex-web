import Playlist from "../data/Playlist.js";
import Storage from "./Storage.js";

class PlaylistStorage extends Storage<string, Playlist> {
  override async fetchItem(id: string): Promise<Playlist> {
    return null;
  }

  constructor() {
    super();

    let json = localStorage.getItem("userPlaylists");
    let userPlaylists: any;

    function generateDefaultPlaylist() {
      userPlaylists = {
        playlists: {},
      };
      json = JSON.stringify(userPlaylists);
    }

    if (json === null) {
      generateDefaultPlaylist();
    } else {
      try {
        userPlaylists = JSON.parse(json);
      } catch (e) {
        generateDefaultPlaylist();
      }
    }

    if (typeof userPlaylists !== "object" || !("playlists" in userPlaylists)) {
      generateDefaultPlaylist();
    }

    for (let key in userPlaylists.playlists) {
      let playlistData = userPlaylists.playlists[key];
      let playlist = new Playlist(
        playlistData.name,
        playlistData.owner,
        playlistData.description,
        playlistData.songs
      );
      this.updateItem(key, playlist);
    }
  }
}

export default PlaylistStorage;
