abstract class Storage<ID, Item> {
  protected items = new Map<ID, Item>();

  get allItems() {
    return this.items;
  }

  abstract fetchItem(id: ID): Promise<Item | null>;

  async getItem(id: ID): Promise<Item | null> {
    if (!this.items.has(id)) {
      this.items.set(id, await this.fetchItem(id));
    }
    return this.items.get(id);
  }

  updateItem(id: ID, item: Item): void {
    this.items.set(id, item);
  }
}

export default Storage;
