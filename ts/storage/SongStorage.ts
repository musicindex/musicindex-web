import Config from "../Config.js";
import Song from "../data/Song.js";
import GETRequest from "../util/GETRequest.js";
import Storage from "./Storage.js";

class SongStorage extends Storage<string, Song> {
  override async fetchItem(id: string): Promise<Song> {
    try {
      let req = new GETRequest(`${Config.apiUrl}/song/${encodeURIComponent(id)}`);
      await req.send();
      return Song.loadData(JSON.parse(req.response)) || null;
    } catch (e) {
      return null;
    }
  }

  constructor() {
    super();
  }
}

export default SongStorage;
