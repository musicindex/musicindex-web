import Config from "../Config.js";
import Album from "../data/Album.js";
import GETRequest from "../util/GETRequest.js";
import Storage from "./Storage.js";

class AlbumStorage extends Storage<string, Album> {
  override async fetchItem(id: string): Promise<Album> {
    try {
      let req = new GETRequest(`${Config.apiUrl}/album/${encodeURIComponent(id)}`);
      await req.send();
      return Album.loadData(JSON.parse(req.response));
    } catch (e) {
      return null;
    }
  }

  constructor() {
    super();
  }
}

export default AlbumStorage;
