import PlaylistStorage from "./storage/PlaylistStorage.js";
import SongStorage from "./storage/SongStorage.js";
import AlbumStorage from "./storage/AlbumStorage.js";
import ArtistStorage from "./storage/ArtistStorage.js";
import MusicManager from "./music/MusicManager.js";
import Util from "./util/Util.js";
import Screen from "./screen/Screen.js";
import ScreenFactory from "./screen/ScreenFactory.js";
import Sidebar from "./component/Sidebar.js";
import HomeScreen from "./screen/HomeScreen.js";
import Player from "./component/Player.js";
import Logger from "./util/Logger.js";

class App {
  playlistStorage = new PlaylistStorage();
  songStorage = new SongStorage();
  albumStorage = new AlbumStorage();
  artistStorage = new ArtistStorage();

  music = new MusicManager();
  currentSong?: string; // Current song ID
  currentPlaylist?: string; // Current playlist ID
  currentAlbum?: string; // Current album ID

  private screens = new Array<Screen>();
  player = new Player();
  sidebar = new Sidebar();



  playSong() {
    this.music.play();

    let player = document.getElementById("player");
    let playButton = player.querySelector("#play-button");
    if (this.music.isPlaying()) {
      playButton.classList.add("playing");
    } else {
      playButton.classList.remove("playing");
    }
  }

  pauseSong() {
    this.music.pause();
    let player = document.getElementById("player");
    let playButton = player.querySelector("#play-button");
    playButton.classList.remove("playing");
  }

  togglePlaying() {
    if (!this.music.isPlaying()) {
      this.playSong();
    } else {
      this.pauseSong();
    }
  }

  #volume = 0;
  get volume() {
    return this.#volume;
  }

  updateVolume() {
    let player = document.getElementById("player");
    let volumeSlider = <HTMLInputElement>player.querySelector("#volume-slider");
    let volume = parseFloat(volumeSlider.value);
    this.music.setVolume(volume);
    this.#volume = volume;
  }

  async previousSong() {
    // [TODO] Change this to use a queue
    if (this.currentSong !== undefined) {
      if (this.currentPlaylist !== undefined) {
        let playlist = await this.playlistStorage.getItem(this.currentPlaylist);
        let newIndex = playlist.songs.findIndex(a => a === this.currentSong) - 1;

        if (newIndex < 0) return;
        await this.playSongInCurrentPlaylist(newIndex);

      } else if (this.currentAlbum !== undefined) {
        let album = await this.albumStorage.getItem(this.currentAlbum);
        let newIndex = album.songs.findIndex(a => a === this.currentSong) - 1;

        if (newIndex < 0) return;
        await this.playSongInCurrentPlaylist(newIndex);
      }
    }
  }

  async nextSong() {
    // [TODO] Change this to use a queue
    if (this.currentSong !== undefined) {
      if (this.currentPlaylist !== undefined) {
        let playlist = await this.playlistStorage.getItem(this.currentPlaylist);
        let newIndex = playlist.songs.findIndex(a => a === this.currentSong) + 1;

        if (newIndex >= playlist.songs.length) return;
        await this.playSongInCurrentPlaylist(newIndex);

      } else if (this.currentAlbum !== undefined) {
        let album = await this.albumStorage.getItem(this.currentAlbum);
        let newIndex = album.songs.findIndex(a => a === this.currentSong) + 1;

        if (newIndex >= album.songs.length) return;
        await this.playSongInCurrentPlaylist(newIndex);
      }
    }
  }


  get currentScreen() {
    return this.screens[this.screens.length - 1];
  }

  popScreen() {
    if (this.currentScreen !== undefined) {
      this.currentScreen.close();
      this.screens.pop();
      this.updateReplaceScreen();
      this.currentScreen.unpause();
    }
  }

  async pushScreen(screen: Screen) {
    if (this.currentScreen !== undefined) {
      this.currentScreen.pause();
    }
    this.screens.push(screen);
    this.updateReplaceScreen();
    await this.currentScreen.open();
  }

  async changeScreen(screen: Screen) {
    if (this.currentScreen !== undefined) {
      this.currentScreen.close();
      this.screens.pop();
    }
    this.screens.push(screen);
    this.updateReplaceScreen();
    await this.currentScreen.open();
  }

  private updateReplaceScreen() {
    let screens = document.getElementsByClassName("screen");
    for (let x of screens) {
      x.classList.remove("active");
    }
    let screenDiv = document.getElementById(this.currentScreen.getScreenDivId_i());
    screenDiv.classList.add("active");

    this.updateHashManually();
  }



  async onHashUpdate(hash: string) {
    let hashArr = hash.split("#");
    let params = new Array<string>();
    for (let hashPart of hashArr) {
      params.push(decodeURIComponent(hashPart));
    }

    let newScreen = ScreenFactory.fromParams(params.slice(1));
    if (newScreen !== undefined) {
      await this.changeScreen(newScreen);
    } else {
      Logger.warn(`Couldn't open new screen with hash "${hash}"`);
    }
  }

  updateHashManually(sendHashchangeEvent = true) {
    let args = this.currentScreen.getParams();
    let hash = this.currentScreen.getId_i();
    for (let x of args) {
      hash += `#${encodeURIComponent(x)}`;
    }
    if (sendHashchangeEvent) {
      location.hash = hash;
    } else {
      history.replaceState(null, null, document.location.pathname + "#" + hash);
    }
  }



  async playSongInCurrentPlaylist(index: number) {
    let playlist = await this.playlistStorage.getItem(this.currentPlaylist);
    let songId = playlist.songs[index];

    await this.changeSong(songId);

    let playlistContainer = document.querySelector("#screen-playlist #list");
    let rows = playlistContainer.querySelectorAll(".song-row");
    rows[index].classList.add("item-playing");
  }

  async changeSong(id: string) {
    this.currentSong = id;
    await this.music.changeSong(id);

    let elems = document.getElementsByClassName("item-playing");
    for (let elem of elems) {
      elem.classList.remove("item-playing");
    }

    let song = await this.songStorage.getItem(id);
    let player = document.getElementById("player");

    let title = <HTMLSpanElement>player.querySelector("#title");
    title.innerText = "";
    title.appendChild(Util.generateDOM_title(id, song === null ? undefined : song.title));

    let artist = <HTMLSpanElement>player.querySelector("#artist");
    artist.innerText = "";
    artist.appendChild(await Util.generateDOM_artist(song === null ? undefined : song.artists));

    this.playSong();
  }



  deselectItem() {
    let elems = document.getElementsByClassName("item-selected");
    for (let elem of elems) {
      elem.classList.remove("item-selected");
    }
  }

  async init() {
    let app = this;

    window.addEventListener("keydown", e => {
      if (e.code === "Space") {
        app.togglePlaying();

      } else if (e.code === "ArrowRight") {
        app.music.setTime(app.music.getTime() + 5);
        this.updateTimeSlider();

      } else if (e.code === "ArrowLeft") {
        app.music.setTime(app.music.getTime() - 5);
        this.updateTimeSlider();
      }
    });

    window.addEventListener("hashchange", async () => {
      await app.onHashUpdate(location.hash);
    });
    if (location.hash !== "") {
      await app.onHashUpdate(location.hash);
    } else {
      await this.changeScreen(new HomeScreen());
    }

    ScreenFactory.init();
    this.player.init();
    this.sidebar.init();

    setInterval(() => {
      app.loop();
    }, 100);
  }

  updateTimeSlider() {
    let player = document.getElementById("player");
    let timeSlider = <HTMLInputElement>player.querySelector("#time-slider");
    if (timeSlider.getAttribute("pressed") !== "true") {
      timeSlider.value = `${this.music.getTime() / this.music.getDuration()}`;
      timeSlider.disabled = !this.music.isPlayable();
    }
  }

  loop() {
    let player = document.getElementById("player");

    if (this.music.isPlaying()) {
      this.updateTimeSlider();
    }

    let currentTime = <HTMLSpanElement>player.querySelector("#current-time");
    currentTime.innerText = Util.formatTime(this.music.getTime());

    let endTime = <HTMLSpanElement>player.querySelector("#end-time");
    endTime.innerText = Util.formatTime(this.music.getDuration());
  }

  static #instance: App = undefined;
  static get i() {
    if (this.#instance === undefined) {
      this.#instance = new App();
    }
    return this.#instance;
  }
}

export default App;
